class AddFlaggedColumnToPhotos < ActiveRecord::Migration
  def self.up
    add_column :photos, :flagged, :boolean
  end

  def self.down
    remove_column :photos, :flagged
  end
end
