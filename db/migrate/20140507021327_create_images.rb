class CreateImages < ActiveRecord::Migration
  def self.up
    create_table :images do |t|
      t.string :caption
      t.text :description
      t.string :url
      t.integer :rating

      t.timestamps
    end
  end

  def self.down
    drop_table :images
  end
end
