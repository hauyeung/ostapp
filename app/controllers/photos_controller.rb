class PhotosController < ApplicationController
def search 
	terms = params[:terms].upcase 
    @photos = Photo.where(["upper(caption) like ? OR upper(description) like ?", "%#{terms}%", "%#{terms}%"]) 
    respond_to do |format| 
      format.html # search.html.erb 
      format.xml  { render :xml => @tasks } 
    end 
  end 

  # GET /photos
  # GET /photos.xml
  def index
    @photos = Photo.where(['flagged == ? or flagged is null', false]) 

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @photos }
    end
  end

  # GET /photos/1
  # GET /photos/1.xml
  def show
    @photo = Photo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @photo }
    end
  end

  # GET /photos/new
  # GET /photos/new.xml
  def new
    @photo = Photo.new
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @photo }
    end
  end

  # GET /photos/1/edit
  def edit
    @photo = Photo.find(params[:id])
  end

  # POST /photos
  # POST /photos.xml
  def create
    @photo = Photo.new(params[:photo])
	@photo.user_id = @user.id
    respond_to do |format|
      if @photo.save
        format.html { redirect_to(@photo, :notice => 'Photo was successfully created.') }
        format.xml  { render :xml => @photo, :status => :created, :location => @photo }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @photo.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /photos/1
  # PUT /photos/1.xml
  def update
    @photo = Photo.find(params[:id])

    respond_to do |format|
      if @photo.update_attributes(params[:photo])
        format.html { redirect_to(@photo, :notice => 'Photo was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @photo.errors, :status => :unprocessable_entity }
      end
    end
    end
    

  # DELETE /photos/1
  # DELETE /photos/1.xml
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to(photos_url) }
      format.xml  { head :ok }
    end
  end
  
  def fivestar
  	@fivestar = Photo.find_all_by_rating(100)
	respond_to do |format|
      	format.html # fivestar.html.erb
      	format.xml  { render :xml => @fivestar}
    end
  end
  
    before_filter :login
    private
  def login
    	authenticate_or_request_with_http_basic("The admin area") do |username, password|
    	@user = User.find_by_username(username)
	@user != nil && password == @user.password     
    end
  end
end
