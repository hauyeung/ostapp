class Photo < ActiveRecord::Base
	belongs_to :user
	validates :caption, :description, :rating, :url, :presence => true
	validates :rating, :numericality => {:greater_than_or_equal_to =>0, :less_than_or_equal_to => 100 }
	validates :caption, :url, :uniqueness => true
end
